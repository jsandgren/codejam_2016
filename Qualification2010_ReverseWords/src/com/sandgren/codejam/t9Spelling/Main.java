package com.sandgren.codejam.t9Spelling;

public class Main {


    // 1. Go through list of items add to list
    // 2. Find 2 items in list matching the credit
    // 3. Output index of items in list

    public static void main(String[] args) {
	// write your code here

        // 1. read in data from file
        // 2. Build test cases
        // 3. Run test cases
        //(new TestCaseRunner("testdata-t9spelling.txt", "result-test-spelling.txt")).run();
        //(new TestCaseRunner("C-small-practice.in", "result-c-small.txt")).run();
        (new TestCaseRunner("C-large-practice.in", "result-c-large.txt")).run();
    }
}
