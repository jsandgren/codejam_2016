package com.sandgren.codejam.t9Spelling;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCase {
    private HashMap<String, String> dictionary = new HashMap<String, String>();

    public TestCase() {
        dictionary.put("a", "2");
        dictionary.put("b", "22");
        dictionary.put("c", "222");
        dictionary.put("d", "3");
        dictionary.put("e", "33");
        dictionary.put("f", "333");
        dictionary.put("g", "4");
        dictionary.put("h", "44");
        dictionary.put("i", "444");
        dictionary.put("j", "5");
        dictionary.put("k", "55");
        dictionary.put("l", "555");
        dictionary.put("m", "6");
        dictionary.put("n", "66");
        dictionary.put("o", "666");
        dictionary.put("p", "7");
        dictionary.put("q", "77");
        dictionary.put("r", "777");
        dictionary.put("s", "7777");
        dictionary.put("t", "8");
        dictionary.put("u", "88");
        dictionary.put("v", "888");
        dictionary.put("w", "9");
        dictionary.put("x", "99");
        dictionary.put("y", "999");
        dictionary.put("z", "9999");
        dictionary.put(" ", "0");
    }

    private String getT9(char c) {
        String retString = this.dictionary.get(String.valueOf(c));

        //System.out.printf("Decoded: %c to %s\n", c, retString);
        return retString;
    }

    public String buildT9String(String message) {
        String t9Code;
        String t9Code_prev = "A";
        String t9String;

        //System.out.printf("Message: %s\n", message);

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            t9Code = getT9(message.charAt(i));
            if (t9Code_prev.charAt(0) == t9Code.charAt(0)) {
                t9Code_prev = t9Code;
                t9Code = (new String(" ")).concat(t9Code);
            }
            else {
                t9Code_prev = t9Code;
            }

            strBuilder.append(t9Code);
        }

        t9String = strBuilder.toString();
        System.out.printf("Message: %s| T9:|%s|\n", message, t9String);

        return t9String;
    }

    public String run(String message)
    {

        return buildT9String(message);
    }
}
