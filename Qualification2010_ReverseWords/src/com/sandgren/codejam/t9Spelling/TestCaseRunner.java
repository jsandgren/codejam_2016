package com.sandgren.codejam.t9Spelling;

import java.io.*;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCaseRunner {

    String filePath;
    String outfilename;
    int testCases;
    BufferedReader br = null;
    BufferedWriter bw = null;

    public TestCaseRunner(String filename, String outfilename) {
        this.filePath = this.getClass().getResource(filename).getFile();
        this.outfilename = outfilename;
        System.out.printf("Found file path %s\n", this.filePath);
    }

    public void run() {
        try {
            this.br = new BufferedReader(new FileReader(this.filePath));
            this.bw = new BufferedWriter(new FileWriter(this.outfilename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        readHeaderInfo();

        for(int i = 1; i <= this.testCases; i++) {
            String testResult = executeTestCase(i);
            System.out.print("FILEWRITE: |" + testResult + "|");

            try {
                bw.write(testResult);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            br.close();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readHeaderInfo() {
        String line1 = null;

        try {
            line1 = this.br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.testCases = Integer.parseInt(line1.trim());
        System.out.printf("Found %d testcases\n", this.testCases);
    }

    private String readNextTestCaseInfo() {
        String message = null;

        try {
            message = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return message;
    }

    private String executeTestCase(int testCaseNbr) {
        String message = readNextTestCaseInfo();

        TestCase tc = new TestCase();
        String retString = tc.run(message);

        return new String("Case #" + testCaseNbr + ": " + retString + "\n");
    }
}
