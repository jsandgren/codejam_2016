package com.sandgren.codejam.alienlanguage;

import java.io.*;
import java.util.HashSet;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCaseRunner {

    String filePath;
    String outfilename;
    int testCases;
    int wordCnt;
    int wordLen;
    BufferedReader br = null;
    BufferedWriter bw = null;
    HashSet<String> alienWordSet;

    public TestCaseRunner(String filename, String outfilename) {
        this.filePath = this.getClass().getResource(filename).getFile();
        this.outfilename = outfilename;
        System.out.printf("Found file path %s\n", this.filePath);
    }

    public void run() {
        try {
            this.br = new BufferedReader(new FileReader(this.filePath));
            this.bw = new BufferedWriter(new FileWriter(this.outfilename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        readHeaderInfo();

        for(int i = 1; i <= this.testCases; i++) {
            String testResult = executeTestCase(i);
            System.out.print("FILEWRITE: |" + testResult + "|");

            try {
                bw.write(testResult);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            br.close();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readHeaderInfo() {
        String line = null;
        String trimmedWord = null;
        alienWordSet = new HashSet<String>();

        try {
            line = this.br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String reg = " {1,}";

        String info[] = line.split(reg);
        this.wordLen = Integer.parseInt(info[0].trim());
        this.wordCnt = Integer.parseInt(info[1].trim());
        this.testCases = Integer.parseInt(info[2].trim());
        System.out.printf("Header info: %d testcases, %d wordCnt, %d wordLen\n", this.testCases, this.wordCnt, this.wordLen);

        for(int i = 0; i < this.wordCnt; i++) {
            try {
                line = this.br.readLine();
                trimmedWord = line.trim().substring(0,this.wordLen);
                alienWordSet.add(trimmedWord);
                System.out.printf("Added %s to dictionary\n", trimmedWord);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String readNextTestCaseInfo() {
        String protoword = null;

        try {
            protoword = br.readLine().trim();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return protoword;
    }

    private String executeTestCase(int testCaseNbr) {
        String protoword = readNextTestCaseInfo();
        TestCase tc = new TestCase(this.alienWordSet, this.wordLen);
        String retString = tc.run(protoword);

        return new String("Case #" + testCaseNbr + ": " + retString + "\n");
    }
}
