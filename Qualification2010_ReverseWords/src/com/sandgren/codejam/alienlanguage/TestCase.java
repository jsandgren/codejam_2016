package com.sandgren.codejam.alienlanguage;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCase {
    private HashSet<String> alienWordSet;
    private int wordLen;

    public TestCase(HashSet<String> set, int wordLen) {
        this.alienWordSet = set;
        this.wordLen = wordLen;
    }

    private String[] getCombo(String[] splitString, int startIndex) {
    	String[] retTmp = null;
    	
//        System.out.printf("Enter getCombo: index:%d", startIndex);
//    	for (String s : splitString) {
//            System.out.printf("|%s|", s);            		
//    	}
//        System.out.printf("\n");    
    	
        try {
        	if (splitString[startIndex].length() == 0) {

        		retTmp = getCombo(splitString, (startIndex+1));

        	} else {   

        		String[] tmp = getCombo(splitString, (startIndex+1));
        		int combos = tmp.length * splitString[startIndex].length();

        		retTmp = new String[combos];
        		int ii = 0;

        		for (char c : splitString[startIndex].toCharArray()) {
    		        //System.out.printf("getCombo: add %c to strings\n", c);      
        			for (String s : tmp) {
        		        //System.out.printf("getCombo: to add %c to %s\n", c, s); 
        		        retTmp[ii] = String.valueOf(c).concat(s);          
        				ii++;
        		        //System.out.printf("getCombo: added %c to %s\n", c, s);      		
        			}
        		    
//        			for (String s : retTmp) {
//        				System.out.printf("getCombo1: %s\n", s);      
//        			}
        		}
        	}
        } catch (Exception e) {
        	String s = "";
    		retTmp = new String[1];
    		retTmp[0] = "";        	
        }        	

//        System.out.printf("*** Exit getCombo: index:%d returning:", startIndex);
//    	for (String s : retTmp) {
//            System.out.printf("|%s|", s);            		
//    	}
//        System.out.printf("\n");    
        
        return retTmp;
    }
    
    private String[] stringSplitter(String rawString, int splits) {
		//return rawString.split("[^a-z]");
    	int ii = 0;
    	boolean inSub = false;
    	String retStrings[] = new String[splits];

    	for (int i=0; i < splits; i++) {
        	retStrings[i]= "";
    	}
    	
    	for (char c : rawString.toCharArray()) {
    		if (!Character.isLetter(c)) {
                //System.out.printf("Changing state from inSub: %s", String.valueOf(inSub));
    			inSub = !inSub;    			
                //System.out.printf("to inSub: %s\n", String.valueOf(inSub));
                if (!inSub) {
        			ii++;        			
                }
    		} else {
    			retStrings[ii] = retStrings[ii].concat(String.valueOf(c));
    			
        		if (inSub) {
                    //System.out.printf("Going into sub %c ii:%d\n", c, ii);
        		} else {
                    //System.out.printf("Going out from sub %c ii:%d\n", c, ii);
        			ii++;        			
        		}
    			
    		}
    		
    	}
    	return retStrings;
    }

    private String[] buildWordTree(String rawString, int splits) {
        int combinations = 1;
        String[] splitString = stringSplitter(rawString, splits);

        System.out.printf("Splitted: |%s| to |", rawString);

        for (String part : splitString) {
            System.out.printf(":%s: (%d)", part, part.length());
        }

        for(int i = 0; i < splitString.length; i++) {
            if (splitString[i].length() > 0) {
                combinations = combinations * splitString[i].length();
            }
        }
        String[] allCombos = getCombo(splitString, 0);
        

        System.out.printf("|\n combinations: %d\n", combinations);

        return allCombos;
    }

    public String run(String protoword)
    {
        String[] wordTree = buildWordTree(protoword, wordLen);
        int actualWords = 0;

        for (String wordCandidate : wordTree) {
            if (this.alienWordSet.contains(wordCandidate)) {
                System.out.printf("This is an alien word: %s\n", wordCandidate);
                actualWords++;
            } else {
                //System.out.printf("This is NOT an alien word: %s\n", wordCandidate);
            }
        }
        return Integer.toString(actualWords);
    }
}
