package com.sandgren.codejam.reversewords;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCase {
    private String reverseString;

    public TestCase() {
    }

    public void addItems(String spaceSeparateditems) {
        int itemIndex = 1;
        String reg = " {1,}";

        String[] items = spaceSeparateditems.split(reg);

        System.out.printf("String list: %d\n", items.length);

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < items.length; i++) {
            strBuilder.append(items[items.length - i - 1] + " ");
        }

        this.reverseString = strBuilder.toString();
    }

    public String run() {
        return this.reverseString;
    }
}
