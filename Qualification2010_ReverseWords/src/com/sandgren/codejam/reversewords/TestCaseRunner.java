package com.sandgren.codejam.reversewords;

import java.io.*;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCaseRunner {

    String filePath;
    String outfilename;
    int testCases;
    BufferedReader br = null;
    BufferedWriter bw = null;

    public TestCaseRunner(String filename, String outfilename) {
        this.filePath = this.getClass().getResource(filename).getFile();
        this.outfilename = outfilename;
        System.out.printf("Found file path %s\n", this.filePath);
    }

    public void run() {
        try {
            this.br = new BufferedReader(new FileReader(this.filePath));
            this.bw = new BufferedWriter(new FileWriter(this.outfilename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        readHeaderInfo();

        for(int i = 1; i <= this.testCases; i++) {
            String testResult = executeTestCase(i);
            System.out.print("FILEWRITE: |" + testResult + "|");

            try {
                bw.write(testResult);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            br.close();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readHeaderInfo() {
        String line1 = null;

        try {
            line1 = this.br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.testCases = Integer.parseInt(line1.trim());
        System.out.printf("Found %d testcases\n", this.testCases);
    }

    private String readNextTestCaseInfo() {
        String itemsString = null;

        try {
            itemsString = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return itemsString;
    }

    private String executeTestCase(int testCaseNbr) {
        String info = readNextTestCaseInfo();

        TestCase tc = new TestCase();
        tc.addItems(info);
        String retString = tc.run();

        return new String("Case #" + testCaseNbr + ": " + retString + "\n");
    }
}
