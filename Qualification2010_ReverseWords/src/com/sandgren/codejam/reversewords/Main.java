package com.sandgren.codejam.reversewords;

public class Main {


    // 1. Go through list of items add to list
    // 2. Find 2 items in list matching the credit
    // 3. Output index of items in list

    public static void main(String[] args) {
	// write your code here

        // 1. read in data from file
        // 2. Build test cases
        // 3. Run test cases
        //(new TestCaseRunner("testdata_reversewords.txt", "result-test.txt")).run();
        //(new TestCaseRunner("B-small-practice.in", "result-B-small.txt")).run();
        (new TestCaseRunner("B-large-practice.in", "result-B-large.txt")).run();
    }
}
