package com.sandgren.codejam.storecredit;

public class Main {


    // 1. Go through list of items add to list
    // 2. Find 2 items in list matching the credit
    // 3. Output index of items in list

    public static void main(String[] args) {
	// write your code here

        // 1. read in data from file
        // 2. Build test cases
        // 3. Run test cases
        (new TestCaseRunner("testdata.txt", "result.txt")).run();
        (new TestCaseRunner("A-small-practice.in", "result-small.txt")).run();
        (new TestCaseRunner("A-large-practice.in", "result-large.txt")).run();
        //TestCaseRunner tcr = new TestCaseRunner("A-small-practice.in", "result.txt");
        //TestCaseRunner tcr = new TestCaseRunner("A-large-practice.in", "result-large.txt");
    }
}
