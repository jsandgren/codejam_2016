package com.sandgren.codejam.storecredit;

import com.sun.javafx.binding.StringFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Johan on 2016-03-10.
 */
public class TestCase {
    private int credit;
    private int[] list;
    private String[] items;


    public TestCase(int credit, int items) {
        this.list = new int[items];
        this.credit = credit;
    }



    public void addItems(String spaceSeparateditems) {
        int itemIndex = 0;
        String reg = " {1,}";

        this.items = spaceSeparateditems.split(reg);

        if (this.list.length != this.items.length) {
            System.out.printf("Sizes don't match\nint list: %d string list: %d\n", this.list.length, this.items.length);
        }

        for (String item : this.items) {
            this.list[itemIndex] = Integer.parseInt(item);
            itemIndex++;
        }
    }

    public void printItems() {
        int itemIndex = 1;
        for (String item : this.items) {
            System.out.printf("%d:\t%s\n", itemIndex, item);
            itemIndex++;
        }
    }
    public void printList() {
        int itemIndex = 1;
        for (int item : this.list) {
            System.out.printf("INT- %d:\t%s\n", itemIndex, item);
            itemIndex++;
        }
    }

    private int[] getMatchingCredit(int[] itemList, int credit) {
        for (int item1 = 0; item1 < (itemList.length - 1); item1++) {
            for (int item2=(item1+1); item2 < itemList.length; item2++) {
                if (itemList[item1] + itemList[item2] == credit) {
                    System.out.printf("Found a match: %d + %d = %d\titemList[%d] + itemList[%d]\n", itemList[item1], itemList[item2], credit, item1, item2);
                    int index[] = {(item1 + 1), (item2 + 1)};
                    return index;
                }
            }
        }

        return null;
    }

    public String run() {
        String returnString;
        int retVal[] = getMatchingCredit(this.list, this.credit);
        returnString = Integer.toString(retVal[0]) + " " + Integer.toString(retVal[1]);

        return returnString;
    }
}
